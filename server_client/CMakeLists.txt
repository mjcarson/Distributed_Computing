cmake_minimum_required(VERSION 3.3)
project(RTS_STUFF)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 ")

set(SOURCE_FILES main.cpp client.cpp client.h server.cpp server.h)
add_executable(RTS_STUFF ${SOURCE_FILES})