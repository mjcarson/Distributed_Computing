#include <string.h>
#include <cstring>
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <strings.h>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <vector>
#include <math.h>
using namespace std;

int kill = 0;

string outgoing_comms(int outgoing_socket, string plaintext)
{
    // This socket is for traffic initiated by the client
    // premessage is message before decryption/encryption
    // garb will be gone once this is functioned out
    char premessage[2048];
    strcpy(premessage, plaintext.c_str());
    cout << premessage << endl;

    write(outgoing_socket, premessage, 2048);
}

string incoming_comms(int incoming_socket)
{
    // This socket is for traffic initiated by the server
    // premessage is message before decryption/encryption
    // garb will be gone once this is functioned out
    char premessage[2048];
    strcpy(premessage, "exit");
    cout << premessage << endl;

    read(incoming_socket, premessage, 2048);
}

int main (int argc, char* argv[])
{
    // The first part of this will be setting up the sockets
    int outgoing_socket, incoming_socket, portNo;
    bool loop = false;
    struct sockaddr_in svrAdd;
    struct hostent *server;

    if(argc < 3)
    {
        cerr<<"Syntax : ./client <host name> <port>"<<endl;
        return 0;
    }

    portNo = atoi(argv[2]);

    if((portNo > 65535) || (portNo < 2000))
    {
        cerr<<"Please enter port number between 2000 - 65535"<<endl;
        return 0;
    }

    //create client sockets
    //outgoing_socket is for client initiated comms
    //incoming_socket is for server initiated comms
    outgoing_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    usleep(1000);
    incoming_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if(outgoing_socket < 0)
    {
        cerr << "Cannot open socket" << endl;
        return 0;
    }

    if(incoming_socket < 0)
    {
        cerr << "Cannot open socket" << endl;
        return 0;
    }

    server = gethostbyname(argv[1]);

    if(server == NULL)
    {
        cerr << "Host does not exist" << endl;
        return 0;
    }

    bzero((char *) &svrAdd, sizeof(svrAdd));
    svrAdd.sin_family = AF_INET;

    bcopy((char *) server -> h_addr, (char *) &svrAdd.sin_addr.s_addr, server -> h_length);

    svrAdd.sin_port = htons(portNo);

    int checker = connect(outgoing_socket,(struct sockaddr *) &svrAdd, sizeof(svrAdd));

    if (checker < 0)
    {
        cerr << "Cannot connect!" << endl;
        return 0;
    }

    checker = connect(incoming_socket,(struct sockaddr *) &svrAdd, sizeof(svrAdd));

    if (checker < 0)
    {
        cerr << "Cannot connect!" << endl;
        return 0;
    }

    // tells server what type of socket is connecting to it
    //outgoing is initiated by the client
    //incoming is initiated by the server
    outgoing_comms(outgoing_socket, "outgoing");
    usleep(1000);
    char premessage[2048];
    strcpy(premessage, "incoming");
    write(incoming_socket, premessage, 2048);
    //End of socket creation

    cout << "exit after this!!" << endl;
    while (1) {
        usleep(10000);
    }
}