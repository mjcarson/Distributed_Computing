//
// Created by michael on 11/2/15.
//

#include "handler.h"
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <fstream>
#include <strings.h>
#include <stdlib.h>
#include <string>
#include <pthread.h>
using namespace std;

class roster
{
public:
    int workers[100] = {0};
    int status[100] = {0};
    int maxthreads = 100;
};


class worker
{
public:
    string addr = {"0.0.0.0"};
    int port = 0;

};

void *socket(void *);
static int connFd;

void *listener (void *dummyPt)
{
    int pId, portNo, listenFd;
    socklen_t len; //store size of the address
    bool loop = false;
    struct sockaddr_in svrAdd, clntAdd;
    pthread_t threadA[51];
    cout << "declrations" << endl;

    // sets listening socket to correct port
    portNo = 8660;

    //creates listening socket
    cout << "creates socket" << endl;
    listenFd = socket(AF_INET, SOCK_STREAM, 0);
    if(listenFd < 0){
        cerr << "Cannot open socket" << endl;
        return 0;
    }

    //socket options
    bzero((char*) &svrAdd, sizeof(svrAdd));
    svrAdd.sin_family = AF_INET;
    svrAdd.sin_addr.s_addr = INADDR_ANY;
    svrAdd.sin_port = htons(portNo);

    //bind socket
    if(bind(listenFd, (struct sockaddr *)&svrAdd, sizeof(svrAdd)) < 0){
        cerr << "Cannot bind" << endl;
        return 0;
    }

    //Server listens for clients and spawns a new thread here for them.
    cout << "server listends" << endl;
    listen(listenFd, 5);
    int noThread = 0;
    while (noThread < 50){
        socklen_t len = sizeof(clntAdd);
        cout << "Listening" << endl;
        //this is where client connects. server will hang in this mode until client connects
        connFd = accept(listenFd, (struct sockaddr *)&clntAdd, &len);
        cout << "after conn accept" << endl;
        if (connFd < 0){
            cerr << "Cannot accept connection" << endl;
            return 0;
        }
        else {
            cout << "Connection successful" << endl;
        }
        pthread_create(&threadA[noThread], NULL, socket, NULL);
        noThread++;
    }
}


void *socket (void *dummyPt)
{
    // premessage is message before decryption/encryption
    // message is plaintext
    cout << "Thread No: " << pthread_self() << endl;
    char premessage[2048];
    bool loop = false;
    int type = -1;
    int garb;
    while(!loop)
    {
        // zeros premessage and reads in ciphertext
        bzero(premessage, 2048);
        read(connFd, premessage, 2048);
        cout << premessage << endl;

        //checks if client is closing the connection
        if(strcmp(premessage, "exit") == 0)
            break;
        else if(strcmp(premessage, "outgoing") == 0)
            type = 0;
        else if(strcmp(premessage, "incoming") == 0){
            type = 1;
            while(type != 3){
                cin >> garb;
            }
        }
    }
    cout << "\nClosing thread and conn" << endl;
    close(connFd);
}

int main()
{
    pthread_t threadA[1];
    cout << "main" << endl;
    // Creates listener thread for new workers
    pthread_create(&threadA[0], NULL, listener, NULL);
    return 0;
}